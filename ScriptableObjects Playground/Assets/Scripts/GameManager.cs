﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public MobController mobController;
    public MobData[] levelData;

    [Range(1,5)]
    public int level = 1;

    void Start()
    {
        if (level > levelData.Length)
        {
            level = levelData.Length;
        }

        mobController.mobData = levelData[level - 1];
        
    }

}

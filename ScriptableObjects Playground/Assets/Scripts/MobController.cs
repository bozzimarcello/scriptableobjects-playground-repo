﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobController : MonoBehaviour
{
    public MobData mobData;

    float timer;
    bool moveLeft = false;

    void Start()
    {
        gameObject.name = mobData.MobName;
        timer = mobData.moveTime;
        GetComponent<SpriteRenderer>().color = mobData.mobColor;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0 )
        {
            Move();
            timer += mobData.moveTime;
        }
    }

    void Move()
    {
        if (!moveLeft)
        {
            transform.position += Vector3.right;
        }
        else
        {
            transform.position += Vector3.left;
        }
        if (Mathf.Abs(transform.position.x)> mobData.limit)
        {
            moveLeft = !moveLeft;
        }
    }
}

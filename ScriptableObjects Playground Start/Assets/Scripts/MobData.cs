﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName="ScriptableObjects/MobData", order =1)]
public class MobData : ScriptableObject
{
    public string mobName;
    public float moveTime;
    public Color mobColor;
    public int limit = 5;
}
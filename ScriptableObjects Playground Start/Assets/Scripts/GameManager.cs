﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public MobController mobController;
    public MobData[] levelData;

    [Range(1,5)]
    public int level = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (level>levelData.Length)
        {
            level = levelData.Length;
        }
        mobController.mobData = levelData[level-1];
    }

}
